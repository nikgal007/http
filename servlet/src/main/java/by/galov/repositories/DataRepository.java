package by.galov.repositories;

import java.util.ArrayList;
import java.util.List;

public class DataRepository {
    public final List<String> names;

    public DataRepository() {
        this.names = new ArrayList<>();
    }


    public List<String> getNames() {
        return names;
    }

    public void addName(String name) {
        this.names.add(name);
    }

    public void remove(int i) {
        this.names.remove(i);
    }
}
