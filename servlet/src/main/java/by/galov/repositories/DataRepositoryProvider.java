package by.galov.repositories;

public class DataRepositoryProvider {
    private final static DataRepository dataRepository = new DataRepository();

    public static DataRepository getDataRepository() {
        return dataRepository;
    }
}
