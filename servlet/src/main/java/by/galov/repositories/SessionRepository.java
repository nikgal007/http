package by.galov.repositories;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

public class SessionRepository {
    private static final Map<String, HttpSession> sessionMap = new HashMap<>();
    public static Map<String, HttpSession> getSessionMap() {
        return sessionMap;
    }
}
