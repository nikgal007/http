package by.galov.repositories;

import by.galov.dto.Login;

import java.util.ArrayList;
import java.util.List;

public class LoginRepository {
    private List<Login> logins = new ArrayList<>();

    public List<Login> getLogins() {
        return logins;
    }

    public void setLogins(List<Login> logins) {
        this.logins = logins;
    }

    public void addLogin(Login login) {
        this.logins.add(login);
    }
}
