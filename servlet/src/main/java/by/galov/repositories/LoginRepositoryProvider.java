package by.galov.repositories;

public class LoginRepositoryProvider {
    private static final LoginRepository loginRepository = new LoginRepository();

    public static LoginRepository getLoginRepository() {
        return loginRepository;
    }
}
