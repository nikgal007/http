package by.galov.session;

import by.galov.repositories.SessionRepository;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.Map;

@WebListener
public class LoginListener implements HttpSessionAttributeListener {
    private Map<String, HttpSession> sessionMap = SessionRepository.getSessionMap();




    @Override
    public void attributeAdded(HttpSessionBindingEvent httpSessionBindingEvent) {
        System.out.println("attr: " + httpSessionBindingEvent.getSession().getAttribute("login"));
        HttpSession session = httpSessionBindingEvent.getSession();
        sessionMap.put(session.getAttribute("login").toString(), session);
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent httpSessionBindingEvent) {
        HttpSession session = httpSessionBindingEvent.getSession();
        sessionMap.remove(session.getAttribute("login").toString());
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {

    }
}
