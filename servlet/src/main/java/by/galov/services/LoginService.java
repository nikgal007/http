package by.galov.services;

import by.galov.dto.Login;
import by.galov.repositories.LoginRepository;
import by.galov.repositories.LoginRepositoryProvider;

import java.util.logging.Logger;

public class LoginService {
    private final static Logger logger = Logger.getLogger(LoginService.class.getName());

    private final LoginRepository loginRepository = LoginRepositoryProvider.getLoginRepository();
    public boolean checkLogin(String login, String pass) {
       logger.info("login : " + login + " pass : " + pass + " logins : " + loginRepository.getLogins());
        return loginRepository.getLogins().stream().filter(log -> log.getName().equals(login))
                .findFirst().map(log -> log.getPass().equals(pass)).orElse(false);
    }

    public void createLogin(String login, String pass) {
        loginRepository.addLogin(new Login(login, pass));
    }
}
