package by.galov.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnection {
    public Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        String url = "jdbc:postgresql://cloud5.clevertec.ru:5432/academy";
        Properties props = new Properties();
        props.setProperty("user","galov");
        props.setProperty("password","galov");
        return DriverManager.getConnection(url, props);
    }
}
