package by.galov.controllers;

import by.galov.db.DbConnection;
import by.galov.repositories.DataRepository;
import by.galov.repositories.DataRepositoryProvider;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/calculator")
public class CalculateController extends HttpServlet {
    private DataRepository dataRepository;
    public void init() throws ServletException {
        dataRepository = DataRepositoryProvider.getDataRepository();
        super.init();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        dataRepository.addName(req.getParameter("foo"));
        req.getSession().setAttribute("foo", dataRepository.getNames());
        resp.sendRedirect("index.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        dataRepository.remove(Optional.ofNullable(req.getParameter("deleteId"))
                .map(Integer::valueOf)
                .orElseThrow(IllegalArgumentException::new));
        resp.sendRedirect("index.jsp");
    }
}
