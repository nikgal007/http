package by.galov.controllers;

import by.galov.repositories.SessionRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@WebServlet("/admin")
public class AdminController extends HttpServlet {
    private Map<String, HttpSession> sessionMap;
    public void init() throws ServletException {
        sessionMap = SessionRepository.getSessionMap();
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Optional.ofNullable(req.getParameter("deleteId"))
                .map(sessionMap::remove)
                .ifPresent(HttpSession::invalidate);
        req.setAttribute("sessionMap", sessionMap.keySet());
        req.getRequestDispatcher("admin.jsp").forward(req, resp);
    }
}
