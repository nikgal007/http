package by.galov.controllers;

import by.galov.dto.Good;
import by.galov.services.LoginService;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginController extends HttpServlet {
    private LoginService loginService;
    @Override
    public void init() {
        loginService = new LoginService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log("check login");
        String login = req.getParameter("login");
        if (loginService.checkLogin(login, req.getParameter("pass"))) {

            log("result start");
            req.getSession().setAttribute("login", login);
            req.getRequestDispatcher("index.jsp").forward(req, resp);
            EntityManagerFactory entityManagerFactory = Persistence
                    .createEntityManagerFactory("academy");
            Good good = new Good(null, "test", 1, 10.1);
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            log("p1 : " + entityManager.contains(good));
            log("result 1 : " + entityManager.find(Good.class, 1));
            entityManager.getTransaction().begin();
            log("result : " + entityManager.find(Good.class, 1));
            entityManager.persist(good);
            log("p2 : " + entityManager.contains(good));
            log(" good : " + good);
            entityManager.getTransaction().commit();
            entityManager.detach(good);
            log("p3 : " + entityManager.contains(good));
            log("p4 : " + entityManager.find(Good.class, good.getId()));
            Good good1 = entityManager.merge(good);
            log("p5 : " + entityManager.contains(good1));
            entityManager.remove(good1);
            log("p6 : " + entityManager.contains(good1));
            entityManagerFactory.close();
        } else {
            resp.sendRedirect("login.jsp");
        }
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log("check login");
        req.getSession().invalidate();
        resp.sendRedirect("login.jsp");
    }
}
