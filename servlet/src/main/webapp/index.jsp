<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>
<h1>Hello, <c:out value = "${sessionScope.login}"/></h1>
<table border = "1">
  <c:forEach var = "i" items="${sessionScope.foo}" varStatus="loop">
      <tr><td><c:out value = "${i}"/></td><td><a href="/calculator?deleteId=${loop.index}">delete</a></td></tr>
  </c:forEach>
</table>
<form action="/calculator" method="post">
    <input type="text" name="foo" />
    <input type="submit" />
</form>
<a href="/login">logout</a>
</body>
</html>